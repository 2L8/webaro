<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LocaleSwitchController extends AbstractController
{
    /**
	 * @Route("/en", name="english")
	 */
	public function switchLanguageEnglishAction() {
		return $this->switchLanguage('en');
	}

	/**
	 * @Route("/de", name="german")
	 */
	public function switchLanguageGermanAction() {
		return $this->switchLanguage('de');
	}

	private function switchLanguage($locale) {
		$this->get('session')->set('_locale', $locale);
		return $this->redirect($this->generateUrl('home'));
	}

}
