# WebAro
**WebAro** is a skelton web application base, containing everything needed to start build your web app fast. Build a blogging system, a content management system, a forum, a shop, and much more.

We use the [Symfony Framework](https://symfony.com) in it's full web skeleton distribution. Staying alighn with Symfony's latest ***LTS*** version.

## Features
* Multilingual (i18n)
  - English (default - primary)
  - German (secondary - as example)
* Legal Page Examples
* Contact page
* European Cookie Law Compliant
* Admin area
* User Administration
* Developer friendly


## License
BSD 3-Clause License. See [LICENSE](LICENSE) for details.


## Documentation
Not much yet. You might want to check the wiki every now and then.


## Project Site
The project details and evetually a forum will be kept at [the 2L8 Projects site](https://2l8-projects.org).
